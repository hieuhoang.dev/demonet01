﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Model.Response
{
    public class Response<T>
    {
        public Response()
        {
            Code = StatusCodes.Status200OK;
        }
        public int Code { get; set; }
        public string Message { get; set; }
        //public string DataError { get; set; }
        public T Data { get; set; }
        public List<T> DataList { get; set; }
    }
}
