﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Model.Role
{
    public class RoleModel : ModelBase
    {
        public string Name { get; set; }
        public string NormalizedName { get; set; }
    }
}
