﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Model.City
{
    public class CityModel : ModelBase
    {
        public string CityName { get; set; }
    }
}
