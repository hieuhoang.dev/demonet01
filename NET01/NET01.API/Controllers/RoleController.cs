﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NET01.Model.City;
using NET01.Model.Response;
using NET01.Model.Role;
using NET01.Repository.City;
using NET01.Repository.Role;

namespace NET01.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleRepository _roleRepository;

        public RoleController(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        [HttpPost]
        [Route("CreateRole")]
        public Response<string> CreateRole(RoleModel model)
        {
            return _roleRepository.CreateRole(model);

        }

        [HttpGet]
        [Route("GetRoleById")]
        public Response<RoleViewModel> GetRoleById(Guid id)
        {
            return _roleRepository.GetRoleById(id);
        }
    }
}
