﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NET01.Model.Response;
using NET01.Model.Role;
using NET01.Model.User;
using NET01.Repository.Role;
using NET01.Repository.User;

namespace NET01.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost]
        [Route("CreateUser")]
        public Response<string> CreateUser(UserModel model)
        {
            return _userRepository.CreateUser(model);

        }
    }
}
