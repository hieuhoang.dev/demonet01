﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NET01.Model.Account;
using NET01.Model.Response;
using NET01.Repository.Account;
using NET01.Repository.City;
using NET01.Repository.Role;

namespace NET01.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IRoleRepository _roleRepository;

        public AccountController(
            IAccountRepository accountRepository,
            IRoleRepository roleRepository)
        {
            _accountRepository = accountRepository;
            _roleRepository = roleRepository;
        }

        [Route("Authenticate")]
        [HttpPost]
        public Response<string> Authenticate(LoginModel model)
        {
            Response<string> res = new Response<string>();

            var rs = _accountRepository.Authenticate(model);
            if (rs.Code != StatusCodes.Status200OK)
            {
                res.Code = StatusCodes.Status401Unauthorized;
                res.Message = rs.Message;
                return res;
            }
            var role = _roleRepository.GetRoleByUserId(rs.Data.Id);
            var newToken = _accountRepository.EncodeSha256(rs.Data, role.Data.NormalizedName);
            res.Data = newToken;
            if (!string.IsNullOrEmpty(model.Token))
                res.Code = StatusCodes.Status200OK;
            return res;
        }
    }
}
