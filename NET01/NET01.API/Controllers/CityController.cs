﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NET01.Model.City;
using NET01.Model.Response;
using NET01.Repository.City;

namespace NET01.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ICityRepository _cityRepository;

        public CityController(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        [HttpPost]
        public Response<string> CreateCity(CityModel model)
        {
            return _cityRepository.CreateCity(model);

        }
    }
}
