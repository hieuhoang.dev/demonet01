﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using NET01.DataLayer.Context;
using NET01.Model.Response;
using NET01.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository.User
{
    public class UserRepository : RepositoryBase<UserRepository>, IUserRepository
    {
        public UserRepository(Net01Context context) : base(context)
        {
        }

        public Response<string> CreateUser(UserModel model)
        {

            Response<string> response = new Response<string>();

            try
            {
                var hasher = new PasswordHasher<UserModel>();
                var userId = Guid.NewGuid();
                NET01.DataLayer.Entity.User user = new NET01.DataLayer.Entity.User();

                user.Id = userId;
                user.UserName = model.UserName.Trim().ToLower();
                user.PasswordHash = hasher.HashPassword(null, model.Password);
                user.FullName = model.FullName;
                user.CreatedBy = model.CreatedBy;
                user.CreatedDate = DateTime.Now;
                user.UpdatedBy = model.UpdatedBy;
                user.UpdatedDate = DateTime.Now;
                user.NormalizedEmail = model.UserName.Trim().ToLower() + "@fpt.com"; ;
                user.PhoneNumberConfirmed = false;
                user.TwoFactorEnabled = false;
                _context.Users.Add(user);

                var _userRole = new IdentityUserRole<Guid>
                {
                    RoleId = model.RoleId,
                    UserId = userId
                };
                _context.UserRoles.Add(_userRole);
                _context.SaveChanges();

                response.Message = "Thêm mới người dùng thành công";
                response.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {

            }

            return response;
        }

        public Response<UserModel> GetUserById(UserModel model)
        {
            throw new NotImplementedException();
        }
    }
}
