﻿using NET01.Model.Response;
using NET01.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository.User
{
    public interface IUserRepository
    {
        Response<string> CreateUser(UserModel model);
        Response<UserModel> GetUserById(UserModel model);
    }
}
