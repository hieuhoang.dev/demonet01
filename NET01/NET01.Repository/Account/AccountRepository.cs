﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using NET01.Common;
using NET01.DataLayer.Context;
using NET01.Model.Account;
using NET01.Model.Response;
using NET01.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository.Account
{
    public class AccountRepository : RepositoryBase<AccountRepository>, IAccountRepository
    {
        private readonly IConfiguration _configuration;
        public AccountRepository(
            Net01Context context,
            IConfiguration configuration
            ) : base(context)
        {
            _configuration = configuration;
        }

        public Response<UserModel> Authenticate(LoginModel model)
        {
            Response<UserModel> res = new Response<UserModel>();
            try
            {
                var user = _context.Users.Where(x => x.UserName == model.UserName && x.IsDeleted == false).FirstOrDefault();
                if (user == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Tài khoản hoặc mật khẩu bị sai!";
                    return res;
                }
                else
                {
                    UserModel userModel = new UserModel();  
                    userModel.UserName = user.UserName;
                    userModel.FullName = user.FullName;
                    userModel.Id = user.Id;
                    res.Code = StatusCodes.Status200OK;
                    res.Message = "Đăng nhập thành công!";
                    res.Data = userModel;
                    return res;
                }

            }
            catch (Exception ex)
            {
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }

        public string EncodeSha256(UserModel user, string role)
        {
            string token = null;
            var key = _configuration["Tokens:Key"];
            var issuer = _configuration["Tokens:Issuer"];
            var expires = _configuration["Tokens:Expires"];
            if (string.IsNullOrEmpty(key))
                key = "ASDFGHJKLZXCVBNMQWERTYUIOP";
            if (string.IsNullOrEmpty(issuer))
                issuer = "NET01";
            if (string.IsNullOrEmpty(expires))
                expires = "1*24*60*60";
            var arr = expires.Split('*');
            double seconds = 1;
            try
            {
                foreach (var item in arr)
                {
                    seconds *= Int32.Parse(item.ToString());
                }
            }
            catch (Exception ex)
            {
                seconds = 1 * 24 * 60 * 60;
            }
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Role, role == null ? "" : role));
            token = TokenUtil.EncodeSha256(claims, key, issuer, seconds);
            return token;
        }
    }
}
