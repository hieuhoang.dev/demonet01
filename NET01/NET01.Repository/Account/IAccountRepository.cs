﻿using NET01.Model.Account;
using NET01.Model.Response;
using NET01.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository.Account
{
    public interface IAccountRepository
    {
        string EncodeSha256(UserModel user, string role);

        Response<UserModel> Authenticate(LoginModel model);
    }
}
