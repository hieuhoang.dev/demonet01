﻿using Microsoft.AspNetCore.Http;
using NET01.DataLayer.Context;
using NET01.DataLayer.Entity;
using NET01.Model.Response;
using NET01.Model.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository.Role
{
    public class RoleRepository : RepositoryBase<RoleRepository>, IRoleRepository
    {
        public RoleRepository(Net01Context context) : base(context)
        {
        }

        public Response<string> CreateRole(RoleModel model)
        {
            Response<string> response = new Response<string>();

            try
            {
                NET01.DataLayer.Entity.Role role = new NET01.DataLayer.Entity.Role();

                role.Name = model.Name;
                role.NormalizedName = model.NormalizedName;
                role.CreatedBy = model.CreatedBy;
                role.UpdatedBy = model.UpdatedBy;
                _context.Roles.Add(role);
                _context.SaveChanges();

                response.Message = "Thêm mới nhóm quyền thành công";
                response.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {

            }

            return response;
        }

        public Response<RoleViewModel> GetRoleById(Guid id)
        {
            Response<RoleViewModel> res = new Response<RoleViewModel>();
            try
            {
                var role = _context.Roles.FirstOrDefault(x => x.Id == id);

                res.Code = StatusCodes.Status200OK;
                res.Data = new RoleViewModel()
                {
                    Id = role.Id,
                    Name = role.Name,
                    NormalizedName = role.NormalizedName
                };
                return res;

            }
            catch (Exception ex)
            {
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin nhóm quyền!";
                return res;
            }
        }

        public Response<RoleViewModel> GetRoleByUserId(Guid userId)
        {
            Response<RoleViewModel> res = new Response<RoleViewModel>();
            try
            {
                var userRole = _context.UserRoles.FirstOrDefault(x => x.UserId == userId);
                var role = _context.Roles.FirstOrDefault(x => x.Id == userRole.RoleId);

                res.Code = StatusCodes.Status200OK;
                res.Data = new RoleViewModel()
                {
                    Id = role.Id,
                    Name = role.Name,
                    NormalizedName = role.NormalizedName
                };
                return res;

            }
            catch (Exception ex)
            {
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin nhóm quyền!";
                return res;
            }
        }
    }
}
