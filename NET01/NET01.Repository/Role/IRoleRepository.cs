﻿using NET01.Model.City;
using NET01.Model.Response;
using NET01.Model.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository.Role
{
    public interface IRoleRepository
    {
        Response<string> CreateRole(RoleModel model);
        Response<RoleViewModel> GetRoleById(Guid id);
        Response<RoleViewModel> GetRoleByUserId(Guid userId);
    }
}
