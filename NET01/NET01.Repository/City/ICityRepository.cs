﻿using NET01.Model.City;
using NET01.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository.City
{
    public interface ICityRepository
    {
        Response<string> CreateCity(CityModel model);
    }
}
