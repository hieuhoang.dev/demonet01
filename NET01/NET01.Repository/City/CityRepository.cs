﻿using Microsoft.AspNetCore.Http;
using NET01.DataLayer.Context;
using NET01.DataLayer.Entity;
using NET01.Model.City;
using NET01.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository.City
{
    public class CityRepository : RepositoryBase<CityRepository>, ICityRepository
    {
        public CityRepository(Net01Context context) : base(context)
        {
        }

        public Response<string> CreateCity(CityModel model)
        {
            Response<string> response = new Response<string>();

            try
            {
                NET01.DataLayer.Entity.City city = new NET01.DataLayer.Entity.City();

                city.CityName = model.CityName;
                city.UpdatedBy = model.UpdatedBy;
                city.CreatedBy = model.CreatedBy;
                _context.Cities.Add(city);
                _context.SaveChanges();

                response.Message = "Thêm mới thành phố thành công";
                response.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
         
            }

            return response;
        }
    }
}
