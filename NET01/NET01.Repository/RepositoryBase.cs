﻿using NET01.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.Repository
{
    public abstract class RepositoryBase<T>
    {
        public readonly Net01Context _context;
        public RepositoryBase(Net01Context context)
        {
            _context = context;
        }
    }
}
