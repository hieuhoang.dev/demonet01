﻿using Microsoft.AspNetCore.Mvc;
using NET01.Common;
using NET01.MVC.ConnectApi;

namespace NET01.MVC.Controllers
{
    public class BaseController : Controller
    {
        public string token
        {
            get
            {
                try
                {
                    var ss = HttpContext.Session.GetObject<string>(CommonConstants.TOKEN);
                    return ss;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                CallWebAPI._token = value;
                HttpContext.Session.SetObject<string>(CommonConstants.TOKEN, value);
            }
        }
    }
}
