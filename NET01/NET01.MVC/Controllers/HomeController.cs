﻿using Microsoft.AspNetCore.Mvc;
using NET01.Model.Account;
using NET01.MVC.Models;
using NET01.MVC.Service.Account;
using NuGet.Common;
using System.Diagnostics;

namespace NET01.MVC.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAccountService _accountService;

        public HomeController(
            ILogger<HomeController> logger,
            IAccountService accountService)
        {
            _logger = logger;
            _accountService = accountService;
        }

        [Route("bang-dieu-khien")]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]

        public IActionResult ActionLogin(LoginModel model)
        {
            var res = _accountService.Login(model);

            //if (res.Code == StatusCodes.Status200OK)
            //{
            //    token = res.Data;
            //    var userInfo = new UserViewModel();
            //    userInfo.UserName = model.UserName;
            //    var userInfoData = _userService.GetUserByUserName(userInfo);
            //    if (userInfoData != null && userInfoData.Data != null)
            //    {
            //        userInfo.Id = userInfoData.Data.Id;
            //        userInfo.FullName = GetPlainText(userInfoData.Data.FullName);
            //        userInfo.UserRoleName = userInfoData.Data.UserRoleName;
            //        userInfo.UserRoleNormalizedName = userInfoData.Data.UserRoleNormalizedName;
            //        userInfo.IsReportBudget = userInfoData.Data.IsReportBudget;
            //        userInfo.IsUploadBudget = userInfoData.Data.IsUploadBudget;
            //    }

            //    HttpContext.Session.SetObject<UserViewModel>(LoginConstant.PERSON_INFO_SESSION, userInfo);
            //    _SessionUser = userInfo;

            //    ////Lấy config
            //    //var config = _userService.GetConfig();
            //    //_Config = config.Data;
            //    _toastNotification.AddSuccessToastMessage(res.Message);

            //    string authId = GenerateAuthId();
            //    HttpContext.Session.SetString("AuthId", authId);
            //    //CookieOptions options = new CookieOptions()
            //    //{
            //    //    Path = "/",
            //    //    HttpOnly = true,
            //    //    Secure = true,
            //    //    SameSite = SameSiteMode.Strict
            //    //};
            //    HttpContext.Response.Cookies.Append("AuthCookie", authId);
            //}
            //else
            //{
            //    _toastNotification.AddErrorToastMessage(res.Message);

            //}
            //Fix tạm thời
            res.Code = 200;
            return Json(res);
        }
    }
}