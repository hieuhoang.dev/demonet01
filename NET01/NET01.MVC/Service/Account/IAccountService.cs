﻿using NET01.Model.Account;
using NET01.Model.Response;

namespace NET01.MVC.Service.Account
{
    public interface IAccountService
    {
        Response<string> Login(LoginModel model);
    }
}
