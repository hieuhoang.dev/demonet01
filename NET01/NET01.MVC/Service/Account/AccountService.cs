﻿using NET01.Model.Account;
using NET01.Model.Response;
using NET01.MVC.ConnectApi;

namespace NET01.MVC.Service.Account
{
    public class AccountService : Service<AccountService>, IAccountService
    {
        public AccountService(CallWebAPI api, ILogger<AccountService> logger, IHttpClientFactory factory, IHttpContextAccessor context) : base(api, logger, factory, context)
        {
        }

        public Response<string> Login(LoginModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = PathApi.AUTHENCATE_LOGIN;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
    }
}
