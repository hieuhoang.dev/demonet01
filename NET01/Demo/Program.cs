﻿using System;
using System.Threading;

// Delegate for the countdown event
public delegate void CountdownEventHandler(string message);

// Countdown clock class
public class CountdownClock
{
    private int seconds;
    private string message;
    public event CountdownEventHandler CountdownEvent;

    public CountdownClock(string message, int seconds)
    {
        this.message = message;
        this.seconds = seconds;
    }

    public void Start()
    {
        Thread.Sleep(seconds * 1000); // Convert seconds to milliseconds

        // Notify observers if there are any registered
        if (CountdownEvent != null)
        {
            CountdownEvent.Invoke(message);
        }
    }
}

// Observer class
public class Observer
{
    private string name;

    public Observer(string name)
    {
        this.name = name;
    }

    public void Subscribe(CountdownClock clock)
    {
        clock.CountdownEvent += DisplayMessage;
    }

    public void Unsubscribe(CountdownClock clock)
    {
        clock.CountdownEvent -= DisplayMessage;
    }

    private void DisplayMessage(string message)
    {
        Console.WriteLine($"[{name}] Received message: {message}");
    }
}

// Main program
public class Program
{
    public static void Main(string[] args)
    {
        Console.Write("Enter the countdown message: ");
        string message = Console.ReadLine();

        Console.Write("Enter the number of seconds to wait: ");
        int seconds = int.Parse(Console.ReadLine());

        CountdownClock clock = new CountdownClock(message, seconds);

        Observer observer1 = new Observer("Observer 1");
        observer1.Subscribe(clock);

        Observer observer2 = new Observer("Observer 2");
        observer2.Subscribe(clock);

        clock.Start();

        observer1.Unsubscribe(clock);
        observer2.Unsubscribe(clock);

        Console.WriteLine("Press any key to exit.");
        Console.ReadKey();
    }
}
