﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NET01.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class updateTblUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 10, 27, 52, 277, DateTimeKind.Local).AddTicks(5929),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 10, 8, 35, 769, DateTimeKind.Local).AddTicks(9736));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 10, 27, 52, 277, DateTimeKind.Local).AddTicks(5665),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 10, 8, 35, 769, DateTimeKind.Local).AddTicks(9429));

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("2d9e1903-2895-4c81-b0c7-f92473552c30"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("5478c72b-bb13-4b7b-ac51-ab9118c8c856"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "Users");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 10, 8, 35, 769, DateTimeKind.Local).AddTicks(9736),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 10, 27, 52, 277, DateTimeKind.Local).AddTicks(5929));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 10, 8, 35, 769, DateTimeKind.Local).AddTicks(9429),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 10, 27, 52, 277, DateTimeKind.Local).AddTicks(5665));

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("5478c72b-bb13-4b7b-ac51-ab9118c8c856"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("2d9e1903-2895-4c81-b0c7-f92473552c30"));
        }
    }
}
