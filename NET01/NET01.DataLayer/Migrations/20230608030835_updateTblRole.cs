﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NET01.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class updateTblRole : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "UpdatedBy",
                table: "Roles",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedBy",
                table: "Roles",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 10, 8, 35, 769, DateTimeKind.Local).AddTicks(9736),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 1, 10, 54, 15, 61, DateTimeKind.Local).AddTicks(4301));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 10, 8, 35, 769, DateTimeKind.Local).AddTicks(9429),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 1, 10, 54, 15, 61, DateTimeKind.Local).AddTicks(4118));

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("5478c72b-bb13-4b7b-ac51-ab9118c8c856"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("de30594a-9a8b-4e3d-9ca5-d0f1f8ba7503"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "Roles",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Roles",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 1, 10, 54, 15, 61, DateTimeKind.Local).AddTicks(4301),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 10, 8, 35, 769, DateTimeKind.Local).AddTicks(9736));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 1, 10, 54, 15, 61, DateTimeKind.Local).AddTicks(4118),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 10, 8, 35, 769, DateTimeKind.Local).AddTicks(9429));

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("de30594a-9a8b-4e3d-9ca5-d0f1f8ba7503"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("5478c72b-bb13-4b7b-ac51-ab9118c8c856"));
        }
    }
}
