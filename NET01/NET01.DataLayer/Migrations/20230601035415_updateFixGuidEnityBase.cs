﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NET01.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class updateFixGuidEnityBase : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 1, 10, 54, 15, 61, DateTimeKind.Local).AddTicks(4301),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 5, 26, 11, 11, 0, 432, DateTimeKind.Local).AddTicks(7478));

            migrationBuilder.AlterColumn<Guid>(
                name: "UpdatedBy",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 1, 10, 54, 15, 61, DateTimeKind.Local).AddTicks(4118),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 5, 26, 11, 11, 0, 432, DateTimeKind.Local).AddTicks(7287));

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedBy",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("de30594a-9a8b-4e3d-9ca5-d0f1f8ba7503"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("4ae62f79-eccc-4931-a1b8-ef5356d11d6a"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 5, 26, 11, 11, 0, 432, DateTimeKind.Local).AddTicks(7478),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 1, 10, 54, 15, 61, DateTimeKind.Local).AddTicks(4301));

            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "City",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 5, 26, 11, 11, 0, 432, DateTimeKind.Local).AddTicks(7287),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 1, 10, 54, 15, 61, DateTimeKind.Local).AddTicks(4118));

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "City",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("4ae62f79-eccc-4931-a1b8-ef5356d11d6a"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("de30594a-9a8b-4e3d-9ca5-d0f1f8ba7503"));
        }
    }
}
