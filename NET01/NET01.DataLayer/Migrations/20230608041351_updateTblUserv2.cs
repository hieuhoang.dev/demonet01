﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NET01.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class updateTblUserv2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "UpdatedBy",
                table: "Users",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedBy",
                table: "Users",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 11, 13, 51, 537, DateTimeKind.Local).AddTicks(7736),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 10, 27, 52, 277, DateTimeKind.Local).AddTicks(5929));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 11, 13, 51, 537, DateTimeKind.Local).AddTicks(7450),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 10, 27, 52, 277, DateTimeKind.Local).AddTicks(5665));

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("1f8887f1-8055-462f-8b81-80a6e4ea4ea6"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("2d9e1903-2895-4c81-b0c7-f92473552c30"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "Users",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Users",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 10, 27, 52, 277, DateTimeKind.Local).AddTicks(5929),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 11, 13, 51, 537, DateTimeKind.Local).AddTicks(7736));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "City",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 6, 8, 10, 27, 52, 277, DateTimeKind.Local).AddTicks(5665),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 6, 8, 11, 13, 51, 537, DateTimeKind.Local).AddTicks(7450));

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "City",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("2d9e1903-2895-4c81-b0c7-f92473552c30"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("1f8887f1-8055-462f-8b81-80a6e4ea4ea6"));
        }
    }
}
