﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET01.DataLayer.Entity
{
    public class City : EntityBase
    {
        public string CityName { get; set; }
    }
}
